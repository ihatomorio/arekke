import logging
import re
import sys
import traceback
import urllib.request
from abc import ABCMeta, abstractmethod
from concurrent import futures

import chromedriver_binary
from book_app.models import Product, Shops
from django.conf import settings
from django.utils import timezone
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import SessionNotCreatedException
from selenium.webdriver.chrome.options import Options

from .exceptions import LoginFailedException
from .exceptions import NoSuchProductPageException
from .exceptions import PageLayoutWrongException
from .exceptions import WrongChromeDriverVersionException

# global variable
_executor = futures.ThreadPoolExecutor(max_workers=4)

class DoujinShop(metaclass=ABCMeta):

    def __init__(self):
        self.driver = None
        self.request_by = None

    @staticmethod
    def UpdateProductInfo(product, set_shop_num):
        from .shops.booth import Booth
        from .shops.dlsite import DLSite
        from .shops.fanza_comic import FanzaComic
        from .shops.fanza_doujin import FanzaDoujin
        from .shops.melonbooks import Melonbooks

        # サイト別に取得する インスタンスの作成
        if 'booth.pm' in product.url:
            doujinshop = Booth()
        elif 'www.dlsite.com' in product.url:
            doujinshop = DLSite()
        elif 'book.dmm.co.jp' in product.url or 'book.dmm.com' in product.url:
            doujinshop = FanzaComic()
        elif 'www.dmm.co.jp/dc/doujin' in product.url:
            doujinshop = FanzaDoujin()
        elif 'www.melonbooks.co.jp' in product.url:
            doujinshop = Melonbooks()
        else:
            product.title = '未対応URL'
            product.save()
            return

        # 店の番号をセット
        if set_shop_num:
            product.shop = doujinshop._GetShopNumber()

        # 取得処理を実行
        try:
            doujinshop.__UpdateProductInfo(product)

            if product.title == '':
                raise PageLayoutWrongException

        # 本のタイトルが取れなかった
        except PageLayoutWrongException as exception:
            logging.basicConfig(filename='/var/log/arekke/error.log', level=logging.ERROR)
            logging.error(exception.msg)

        # ChromeDriverが古い
        except WrongChromeDriverVersionException as exception:
            logging.basicConfig(filename='/var/log/arekke/error.log', level=logging.ERROR)
            logging.error(exception.msg)

        # 商品が無い場合
        except NoSuchProductPageException:
            # loadingまたは取得失敗の場合のみ更新
            if( product.title == 'loading' or product.title == '取得失敗' ):
                product.title = '商品ページなし'

        # その他の例外
        except Exception as exception:
            logging.basicConfig(filename='/var/log/arekke/critical.log', level=logging.CRITICAL)
            logging.critical(str(exception))
            logging.critical(traceback.format_exc())

            if doujinshop.driver:
                doujinshop.driver.save_screenshot('error.png')


        # 取得結果の保存
        product.save()

    @staticmethod
    def GetProductList(account, request_by):
        # from .shops.booth import Booth
        from .shops.dlsite import DLSite
        from .shops.fanza_comic import FanzaComic
        from .shops.fanza_doujin import FanzaDoujin
        from .shops.melonbooks import Melonbooks

        # shop属性でインスタンスを切り替え
        if account.shop == Shops.DLSITE:
            doujinshop = DLSite()
        elif account.shop == Shops.FANZA_COMIC:
            doujinshop = FanzaComic()
        elif account.shop == Shops.FANZA_DOUJIN:
            doujinshop = FanzaDoujin()
        elif account.shop == Shops.MELONBOOKS:
            doujinshop = Melonbooks()
        else:
            return

        # インスタンスに対して商品一覧取得を開始
        try:
            doujinshop.__GetProductList(account, request_by)

        # ChromeDriverが古い
        except WrongChromeDriverVersionException as exception:
            logging.basicConfig(filename='/var/log/arekke/error.log', level=logging.ERROR)
            logging.error(exception.msg)

        # その他の例外
        except Exception as exception:
            logging.basicConfig(filename='/var/log/arekke/critical.log', level=logging.CRITICAL)
            logging.critical(str(exception))
            logging.critical(traceback.format_exc())

        # 取得完了の時間を入れる
        account.date = timezone.now()
        account.save()

    @staticmethod
    def CreateFromUrl(url, request_by, set_shop_num=True, title=None, circle=None, bought_date=None):
        # URL被りがあったら生成中断
        if Product.objects.filter(owner=request_by, url=url):
            return

        # 商品を生成
        product = Product.objects.create(
            title = 'loading',
            owner = request_by,
            shop = 0,
            url = url
        )

        # 並列処理で商品情報を取得する
        try:
            instance = DoujinShop.UpdateProductInfo(product, set_shop_num)

        # ChromeDriverが古い
        except WrongChromeDriverVersionException as exception:
            logging.basicConfig(filename='/var/log/arekke/error.log', level=logging.ERROR)
            logging.error('critical: ' + exception.msg)

        except Exception as exception:
            if instance.driver:
                instance.driver.save_screenshot('error.png')
            logging.basicConfig(filename='/var/log/arekke/critical.log', level=logging.CRITICAL)
            logging.critical(str(exception))
            logging.critical(traceback.format_exc())

        if product.title == '商品ページなし':
            product.title = title
            product.circle = circle

        product.bought_date = bought_date

        product.save()

    def __UpdateProductInfo(self, product):
        logging.basicConfig(filename='/var/log/arekke/info.log', level=logging.INFO)
        logging.info('Update: ' + product.url)
        # ブラウザを開く
        self.__OpenBrowser()
        self.driver.get(product.url)

        # ページのタイトルを確認する
        try:
            self._CheckOpened()
        except PageLayoutWrongException:
            logging.basicConfig(filename='/var/log/arekke/error.log', level=logging.ERROR)
            logging.error('Site: ' + Shops.getStopString(self._GetShopNumber()) + '_CheckOpened Wrong Page Layout')
            raise

        # 商品名を取得
        try:
            product.title = self._GetTitle()
        except NoSuchElementException:
            product.title = ''

        try:
            # ショップ名称を取得
            product.circle = self._GetCircle()
        except NoSuchElementException:
            product.circle = ''

        try:
            # 作家名を取得
            product.author = self._GetAuthor()
        except NoSuchElementException:
            product.author = ''

        try:
            # 画像を取得
            image_url = self._GetImageUrl()
            if image_url != None:
                product.image_path = self.__GetImage(image_url)
        except NoSuchElementException:
            product.image_url = ''

        # ブラウザを閉じる
        self.__CloseBrowser()

    def __GetProductList(self, account, request_by):
        self.request_by = request_by
        logging.basicConfig(filename='/var/log/arekke/info.log', level=logging.INFO)

        # ブラウザを生成
        self.__OpenBrowser()

        # ログインする
        logging.info('making login')
        self._MakeLogin(account.user, account.password)

        # ログインを確認
        logging.info('checking login')
        try:
            self._CheckLogin()
        except LoginFailedException:
            logging.info('Site: ' + Shops.getStopString(self._GetShopNumber()) + 'User:' + request_by + ' login failed')
            raise #re-raise
        logging.info('Site: ' + Shops.getStopString(self._GetShopNumber()) + 'User:' + request_by + ' login succeeded')

        # 商品URL一覧を取得し商品を作成
        logging.info('getting product')
        self._CreateFromProductList()
        logging.info('getting product end')

        # ブラウザを閉じる
        self.__CloseBrowser()

    def _QueueCreateProduct(self, url, title=None, circle=None, bought_date=None):
        logging.basicConfig(filename='/var/log/arekke/info.log', level=logging.INFO)
        logging.info('Queued:' + url)
        assert self.request_by != None
        _executor.submit(fn=DoujinShop.CreateFromUrl, url=url, request_by=self.request_by, title=title, circle=circle, bought_date=bought_date)

    # ブラウザの生成
    def __OpenBrowser(self):
        options = Options()
        options.binary_location = '/usr/bin/google-chrome'
        options.add_argument('--headless')
        options.add_argument('--no-sandbox') #rootに必要
        try:
            self.driver = webdriver.Chrome(options=options)
        except SessionNotCreatedException as exception:
            if 'This version of ChromeDriver only supports Chrome version' in exception.msg:
                raise WrongChromeDriverVersionException from exception
        self.driver.implicitly_wait(30)

    # ブラウザ終了
    def __CloseBrowser(self):
        self.driver.quit()

    @abstractmethod
    def _CheckOpened(self):
        pass

    @abstractmethod
    def _GetShopNumber(self):
        pass

    @abstractmethod
    def _GetTitle(self):
        pass

    @abstractmethod
    def _GetCircle(self):
        pass

    @abstractmethod
    def _GetAuthor(self):
        pass

    def __GetImage(self, image_url):
        # 保存用パスを生成
        image_path = self._GetImagePath(image_url)

        # 画像をダウンロード
        save_path = self.__GetSavePath(image_path)
        self.__DownloadImage(image_url, save_path)

        # 画像の保存パスを返す
        return image_path

    @abstractmethod
    def _GetImageUrl(self):
        pass

    @abstractmethod
    def _GetImagePath(self, image_url):
        pass

    def __GetSavePath(self, image_path):
        # settings.MEDIA_ROOT = '/root/repos/websq/book_project/media'
        return settings.MEDIA_ROOT + '/' + image_path

    def __DownloadImage(self, image_url, save_path):
        urllib.request.urlretrieve(image_url, save_path)

    @abstractmethod
    def _GetLoginUrl(self):
        pass

    @abstractmethod
    def _GetProductListUrl(self):
        pass

    @abstractmethod
    def _MakeLogin(self, user_name, password):
        pass

    @abstractmethod
    def _CheckLogin(self):
        pass

    @abstractmethod
    def _CreateFromProductList(self):
        pass

    def _SupressSuffixedBracket(self, text):
        # 正規表現を試みる
        matched = re.findall(r'(.*?)(【.*】)+$', text)
        if len(matched) == 0:
            # マッチ文字列なし
            return text
        else:
            # マッチの先頭パターンのみ返す
            return matched[0][0]

    def _GetDetailFromTable(self, title_elements, detail_elements, requirement):
        # title_elementsについて入っているものチェックし、requirementのときその内容を返す
        for title_element, detail_element in zip(title_elements, detail_elements):
            if title_element.text == requirement:
                return detail_element.text
        else:
            return None
