

class LoginFailedException(Exception):
    pass


class NoSuchProductPageException(Exception):
    pass


class PageLayoutWrongException(Exception):
    pass


class WrongChromeDriverVersionException(Exception):
    pass
